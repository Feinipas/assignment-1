function darkMode(){
	
	var a = document.getElementById("wrapper");
	var b = document.getElementById("banner-wrapper");
	var c = document.getElementsByClassName("button button-big");
	var d = document.getElementById("header-wrapper");
	var e = document.getElementById("copyright");
	var x = document.getElementsByTagName("a");
	var y = document.getElementById("height");
	var z = document.getElementById("weight");	
	var f = document.getElementsByTagName("input");
	
	a.style.background = "black";
	a.style.fontColor = "white";

	b.style.background = "black";
	b.style.fontColor = "white";

	c[0].style.background = "#1C1C1C";
	c[0].style.color = "grey";

	d.style.background = "#1C1C1C";


	x[0].style.color = "grey";
	x[1].style.color = "grey";
	x[2].style.color = "grey";
	x[3].style.color = "grey";
	x[4].style.color = "grey";
	x[5].style.color = "grey";

	e.style.background = "#1C1C1C";

	y.style.background = "grey";
	z.style.background = "grey";

	f[2].style.background = "grey";
	f[2].style.color = "black";	
}

var click = 0;

	function clickCount(){
	var field = document.getElementsByClassName("button button-big");
	field[0].innerHTML = click;
	if(click <= 20) {
		click++;
		

	}
	else {
		window.alert("You're the winner! ;) ");

	}
	
}

function countBmi(){

	var height = Number(document.getElementById("height").value);
	var weight = Number(document.getElementById("weight").value);
	var bmi = weight / (height * height);
	if (isNaN(height)) window.alert("Wrong submission. For your height, please enter a number with a point betweenn, like this: 1.85");
	if (isNaN(weight)) window.alert("Wrong submission. For your weight, please enter an integer");

	if (bmi < 18.5) document.getElementById("comment").innerHTML = "To low";
    if (bmi >= 18.5 && bmi <= 25) document.getElementById("comment").innerHTML = "Normal"
    if (bmi > 25) document.getElementById("comment").innerHTML = "To high";   

	document.getElementById("output").innerHTML = Math.round(bmi * 100)/100;

}

function validation() {

	if (document.getElementById("prename").value == "") {
		window.alert("Prename is required");
		
	}

	if (document.getElementById("name").value == "") {
		window.alert("Name is required");
		
	}

	if (document.getElementById("tel").value == "") {
		window.alert("Tel is required");
		
	}

	if (document.getElementById("password").value == "") {
		window.alert("Password is required");
		
	}
	if (document.getElementById("password").value.length <= 5) {
		window.alert("Password too short, at least 5 characters");
		return false;
	}

}

	var nye = new Date("Jan 01, 2021 00:00:00").getTime();

	setInterval(function(){

	var now = new Date().getTime();

	var left = nye - now;

	var days = Math.floor(left / (1000 * 60 * 60 * 24));
  	var hours = Math.floor((left % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
 	var minutes = Math.floor((left % (1000 * 60 * 60)) / (1000 * 60));
  	var seconds = Math.floor((left % (1000 * 60)) / 1000);

	  document.getElementById("days").innerHTML = days;
	  document.getElementById("hours").innerHTML = hours;
	  document.getElementById("minutes").innerHTML = minutes;
	  document.getElementById("seconds").innerHTML = seconds;

	}, 1000)


